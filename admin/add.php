<?php

session_start();

if($_SESSION["admin_login"] != "dnadmin")
	header("location:index.php"); 
	
include_once('connection.php');
$DB_conn=getDB();
$title=$_POST['title'];
$detail=$_POST['content'];
$image=$_FILES["image"]["name"];

$stmt=$DB_conn->prepare("INSERT into blog (title,detail,image,date) values (:title,:detail,:image,:date)");
$stmt->bindParam("title", $title);
$stmt->bindParam("detail", $detail);
$stmt->bindParam("image", $image);
$stmt->bindParam("date", date("Y-m-d"));
$stmt->execute();

if( $_FILES['image']['name'] != "" )
{
	move_uploaded_file($_FILES['image']['tmp_name'],"../img/".$_FILES['image']['name']);
}
header("location:display.php");
?>