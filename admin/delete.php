<?php
session_start();

if($_SESSION["admin_login"] != "dnadmin")
	header("location:index.php"); 


include_once('connection.php');
$DB_conn=getDB();
$id=$_GET["id"];
$stmt=$DB_conn->prepare("delete from blog where id=:id");
$stmt->bindParam(":id", $id);
$stmt->execute();
header("location:display.php");

?>