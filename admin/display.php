<?php

session_start();

if($_SESSION["admin_login"] != "dnadmin")
    header("location:index.php"); 

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include_once('head.php'); ?>
    </head>
    <body>
        <?php include_once('sidebar.php');?>
        <main>
        	<div class="container">

        		<div class="row">
          
                <div class="col s12">
                    <div class="card row">
                        <div class="card-content col s12 primary-text-gradient white-text">
                            <div class="card-title">Post We Had</div>
                        </div>
                        <div class="card-content col s12">
                            <table class="centered">
                                <thead>
                                    <tr>
                                        <th>Sr no.</th>
                                        <th>Title</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    include_once('connection.php');
                                    $con=getDB();
                                    $count=0;
                                    $st=$con->prepare("select * from blog");
                                    $st->execute();
                                    $res=$st->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($res as $row) 
                                    {
                                        $count++;
                                ?>
                                        <tr>
                                            <td><?php echo $count; ?></td>
                                            <td><?php echo $row['title']; ?></td>
                                            <td><a href="delete.php?id=<?php echo $row['id']; ?>">Delete</a></td>
                                        </tr>
                                <?php 
                                    } 
                                ?>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
        		

        	</div>
        	



        </main>
        <?php include_once('footer.php'); ?>

        <script>
    ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .catch( error => {
            console.error( error );
        } );
</script>
    </body>
</html>