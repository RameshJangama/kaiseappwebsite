<footer class="page-footer">
    <div class="container">
            
    </div>
    <div class="footer-copyright">
        <div class="container">
            © 2018 Copyright Techdesire
        </div>
    </div>
</footer>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script>
    $(document).ready(function(){
        $(".button-collapse").sideNav();
        $(".dropdown-button").dropdown({
            gutter: 0, // Spacing from edge
            belowOrigin: true, // Displays dropdown below the button
        });
    })
</script>
