<meta name = "viewport" content = "width = device-width, initial-scale = 1">
<link rel="stylesheet" type="text/css" href="css/materialize.css">
<link rel="stylesheet" type="text/css" href="css/animate.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">



<style>
    body {
        display: flex;
        min-height: 100vh;
        flex-direction: column;
    }

    main {
        flex: 1 0 auto;
    }

    @media only screen and (max-width : 992px) {
        header, main, footer {
            padding-left: 0;
        }
    }
    .btn-100{
        width:100%;
    }
    .userview{
        height:150px;
        background:url('img/userview-bg.jpg') no-repeat;
        padding:16px;
        overflow-y:hidden;
        line-height:30px;
    }
    .userview > .profilepic{
        height:64px;
        width:64px;
        margin-bottom:10px;
    }
    .primary-text-gradient{
        background: #0d47a1;
        background: -moz-linear-gradient(left, #0d47a1 0%, #ec407a 100%);
        background: -webkit-linear-gradient(left, #0d47a1 0%,#ec407a 100%);
        background: linear-gradient(to right, #0d47a1 0%,#ec407a 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0d47a1', endColorstr='#ec407a',GradientType=1 );
    }
    .inline-icon{
        vertical-align:bottom !important;
    }
</style>
