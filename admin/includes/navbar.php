<nav>
    <div class="nav-wrapper">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l2">
                    <a href="#!" class="brand-logo">Logo</a>
                    <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                </div>
                <div class="col l6 hide-on-med-and-down">
                    <form>
                    <div class="input-field">
                      <input id="search" type="search" required>
                      <label class="label-icon left" for="search"><i class="material-icons">search</i>
                      </label>
                      <i class="material-icons closed">close</i>
                    </div>
                  </form>
                </div>
                <div class="col l4">
                    <ul class="dropdown-content" id="family-menu">
                        <li><a href="create_family.php">Create Family</a></li>
                        <li><a href="add_member.php">Add Member To Family</a></li>
                        <li class="divider"></li>
                        <li><a href="#!">List Families</a></li>
                        <li><a href="#!">List Family Members</a></li>
                    </ul>
                    <ul class="dropdown-content" id="sip-stp-menu">
                        <li><a href="create_sip_stp.php">New SIP/STP</a></li>
                        <li><a href="#!">All SIP/STPs</a></li>
                    </ul>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="#" class="dropdown-button" data-activates="family-menu">Family</a></li>
                        <li><a href="#" class="dropdown-button" data-activates="sip-stp-menu">SIP/STP</a></li>
                        <li><a href="#"><i class="material-icons">power_settings_new</i></a></li>
                    </ul>
                </div>
            </div>
        </div>

        <ul class="side-nav" id="mobile-demo">
            <ul class="dropdown-content" id="family-menu2">
                <li><a href="create_family.php">Create Family</a></li>
                <li><a href="add_member.php">Add Member To Family</a></li>
                <li class="divider"></li>
                <li><a href="#!">List Families</a></li>
                <li><a href="#!">List Family Members</a></li>
            </ul>
            <ul class="dropdown-content" id="sip-stp-menu2">
                <li><a href="create_sip_stp.php">New SIP/STP</a></li>
                <li><a href="#!">All SIP/STPs</a></li>
            </ul>
            <li><a href="#" class="dropdown-button" data-activates="family-menu2">Family</a></li>
            <li><a href="#" class="dropdown-button" data-activates="sip-stp-menu2">SIP/STP</a></li>
        </ul>
    </div>
</nav>
