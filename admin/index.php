<?php
session_start();

if($_SESSION["admin_login"] != "dnadmin")
    header("location:index.php"); 
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include_once('head.php'); ?>
    </head>
    <body>
        <?php include_once('sidebar.php');?>
        <main>
        	<div class="container">

        		<form action="add.php" method="post" enctype="multipart/form-data">
	        	
		        	<input type="text" name="title" placeholder="Enter Title">
		        	<input type="text" name="content" placeholder="Enter Detail">
		    		<input type="file" name="image" />
                    <br>
		    		<input type="submit" style="margin-top: 10px;" class="btn" value="Add" name="">
    			</form>
        		

        	</div>
        	



        </main>
        <?php include_once('footer.php'); ?>

        <script>
    ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .catch( error => {
            console.error( error );
        } );
</script>
    </body>
</html>