﻿
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <?php include_once('head.php'); ?>
</head>

<body>

	<form action="login_process.php" method="post" >

    <div class="main mainContentMargin">

        <div class="row">

            <div class="col s8 push-s2 ">

                <div class="card large" style="margin-top:0px">

                    <div class="card-title center-align indigo light white-text" style="font-weight:bold">

                        Login

                    </div>

                    <div class="card-content" style="margin-top:10%">

                        <div class="input-field">

                          <i class="material-icons prefix">account_circle</i>

                          <input id="icon_prefix" type="text" class="validate" name="uname" autocomplete="off">

                          <label for="icon_prefix">Login Id</label>

                        </div>

                        <div class="input-field">

                          <i class="material-icons prefix">https</i>

                          <input id="icon_prefix" type="password" class="validate" name="pwd" autocomplete="off">

                          <label for="icon_prefix">Login Password</label>

                        </div>



                    </div>



                    <div class="card-action center-align">

                            <input type="submit" value="Login" name="submit"  class="btn-large hoverable indigo white-text"/>

                    </div>

                </div>

            </div>

        </div>

    </div>

	</form>

    <?php include_once ("footer.php") ?>

</body>

</html>