-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 04, 2018 at 02:08 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kaise_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `login_id` varchar(100) NOT NULL,
  `pwd` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `login_id`, `pwd`) VALUES
(1, 'kaise', 'kaise@123');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `detail` varchar(50000) NOT NULL,
  `image` varchar(500) NOT NULL,
  `date` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `title`, `detail`, `image`, `date`) VALUES
(1, 'text text', 'Consectetur consectetur commodo consectetur nulla id consequat voluptate. Cillum non nostrud est anim veniam laborum sunt. Incididunt dolor elit aute anim ullamco eiusmod reprehenderit fugiat ipsum. Esse duis elit magna tempor. Mollit eiusmod nulla consectetur pariatur irure elit dolore amet dolor aute aliquip anim sit cillum. Nisi consequat quis laboris occaecat do officia dolore irure reprehenderit elit tempor. Officia et est duis aliquip laborum esse do sint velit do adipisicing officia. \nCupidatat Lorem enim do aliquip. Dolore ea incididunt id proident excepteur dolor eu mollit. Veniam aute mollit aliqua mollit labore dolore adipisicing mollit consectetur mollit cupidatat ea veniam tempor. Labore ut officia officia sint adipisicing ex velit excepteur enim fugiat laboris in tempor qui. Quis fugiat aute amet aute id esse dolor proident elit sit consectetur ipsum. Non mollit incididunt cillum tempor reprehenderit aute excepteur ex. Quis pariatur consectetur elit aliquip reprehenderit aute est proident nisi et fugiat elit. ', '1_Welcome_Screen.png', ''),
(2, 'text text text', 'Consectetur consectetur commodo consectetur nulla id consequat voluptate. Cillum non nostrud est anim veniam laborum sunt. Incididunt dolor elit aute anim ullamco eiusmod reprehenderit fugiat ipsum. Esse duis elit magna tempor. Mollit eiusmod nulla consectetur pariatur irure elit dolore amet dolor aute aliquip anim sit cillum. Nisi consequat quis laboris occaecat do officia dolore irure reprehenderit elit tempor. Officia et est duis aliquip laborum esse do sint velit do adipisicing officia. \nCupidatat Lorem enim do aliquip. Dolore ea incididunt id proident excepteur dolor eu mollit. Veniam aute mollit aliqua mollit labore dolore adipisicing mollit consectetur mollit cupidatat ea veniam tempor. Labore ut officia officia sint adipisicing ex velit excepteur enim fugiat laboris in tempor qui. Quis fugiat aute amet aute id esse dolor proident elit sit consectetur ipsum. Non mollit incididunt cillum tempor reprehenderit aute excepteur ex. Quis pariatur consectetur elit aliquip reprehenderit aute est proident nisi et fugiat elit. ', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
